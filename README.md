# CRUD OOP PHP



## Getting started

in your local computer please run crud-oop-php/endpoint/listdata.php, this index or first page 

## case

This program still has many shortcomings, some validation needs to be added

- **Index of Page**: This list of employee;

    ![](dist/index.PNG)

- **Form new**: This list form create new employee;

    ![](dist/create.PNG)

- **Form update**: This list form update employee;

    ![](dist/update.PNG)