<?php
  include '../connection/endpoint.php';
  $db = new Endpoint();

  $first_name = $_POST['first_name'];
  $last_name = $_POST['last_name'];
  $position = $_POST['position'];
  $office = $_POST['office'];
  $id = $_POST['idKaryawan'];

  if (!empty($id)) {
    # code...
    // echo "update";
    $data = array(
      'first_name' => $first_name,
      'last_name' => $last_name,
      'position' => $position,
      'office' => $office,
      'id' => $id
    );

    $create = $db->updateKaryawan('karyawan', $data);

    if ($create == 1) {
      # code...
      // echo "success update";
      echo "<script> alert('DATA KARYAWAN BERHASIL DI UPDATE');
      location = 'listdata.php'; </script>";
    }else{
      // echo "update failed";
      echo "<script> alert('DATA KARYAWAN GAGAL DI UPDATE');
      location = 'listdata.php'; </script>";
    }

  }else{
    // echo "create";
    $data = array(
      'first_name' => $first_name,
      'last_name' => $last_name,
      'position' => $position,
      'office' => $office
    );
    // print_r($data); die();
    $create = $db->insertKaryawan('karyawan', $data);

    if ($create == 1) {
      # code...
      // echo "success insert";
      echo "<script> alert('DATA KARYAWAN BERHASIL DI INPUT');
      location = 'listdata.php'; </script>";
    }else{
      // echo "insert failed";
      echo "<script> alert('DATA KARYAWAN GAGAL DI INPUT');
      location = 'listdata.php'; </script>";
    }


  }
