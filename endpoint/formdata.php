<?php 
  include '../connection/endpoint.php';
  $db = new Endpoint();
  include '../template/header.php';
  $id = $_GET['id'];

  $getListTabel='';
  if (!empty($id)) {
    # code...
    $getListTabel = $db->getById('karyawan', 'id_karyawan', $id);
?>
  <div class="content">

    <div class="let-form">
      
      <h2 class="title-form">Form Update employee</h2>

      <form action="prosesformdata.php" method="post">
        <div class="mb-3">
          <!-- static value -->
          <input type="hidden" name="idKaryawan" value="<?php echo $getListTabel['id_karyawan']; ?>">
          <!-- end -->
          <label for="firstName" class="form-label">First Name</label>
          <input type="text" class="form-control" id="EmployFirstName" aria-describedby="EmployFirstName" name="first_name" value="<?php echo $getListTabel['first_name']; ?>">
          <!-- <div id="emailHelp" class="form-text">We'll never share your email with anyone else.</div> -->
        </div>
        <div class="mb-3">
          <label for="lastName" class="form-label">Last Name</label>
          <input type="text" class="form-control" id="EmployLastName" name="last_name" value="<?php echo $getListTabel['last_name']; ?>">
          <!-- <div id="emailHelp" class="form-text">We'll never share your email with anyone else.</div> -->
        </div>
        <div class="mb-3">
          <label for="Position" class="form-label">Position</label>
          <select name="position" id="position" class="form-control">
            <option value="1" <?php if($getListTabel['position']=="1") echo 'selected="selected"'; ?> >Staff</option>
            <option value="2" <?php if($getListTabel['position']=="2") echo 'selected="selected"'; ?> >Office Boy</option>
            <option value="3" <?php if($getListTabel['position']=="3") echo 'selected="selected"'; ?> >Secertary</option>
            <option value="4" <?php if($getListTabel['position']=="4") echo 'selected="selected"'; ?> >Administrator</option>
          </select>
          <!-- <div id="emailHelp" class="form-text">We'll never share your email with anyone else.</div> -->
        </div>
        <div class="mb-3">
          <label for="office" class="form-label">Office</label>
          <select name="office" id="office" class="form-control">
            <option value="Jakarta" <?php if($getListTabel['office']=="Jakarta") echo 'selected="selected"'; ?>>Jakarta</option>
            <option value="Bogor" <?php if($getListTabel['office']=="Bogor") echo 'selected="selected"'; ?>>Bogor</option>
            <option value="Bandung" <?php if($getListTabel['office']=="Bandung") echo 'selected="selected"'; ?>>Bandung</option>
            <option value="Surabaya" <?php if($getListTabel['office']=="Surabaya") echo 'selected="selected"'; ?>>Surabaya</option>
          </select>
          <!-- <div id="emailHelp" class="form-text">We'll never share your email with anyone else.</div> -->
        </div>
        <div class="mb-3 form-check">
          <input type="checkbox" disabled class="form-check-input" id="exampleCheck1">
          <label class="form-check-label" for="exampleCheck1">Send email</label>
        </div>
        <!-- <button type="cancel" style="float:right;" class="btn btn-success btn-sm">Cancel</button> -->
        <div class="mb-3" style="float:right;">
          <button type="submit" class="btn btn-success btn-sm">Update</button>
          <a href="listdata.php" class="btn btn-danger btn-sm">Cancel</a>
        </div>
        
      </form>

    </div>

  </div>

<?php 
  }else{
?>
  <div class="content">

    <div class="let-form">

      <h2 class="title-form">Form create new employee</h2>

      <form action="prosesformdata.php" method="post">
        <div class="mb-3">
          <!-- static value -->
          <input type="hidden" value="" name="idKaryawan">
          <!-- end -->
          <label for="firstName" class="form-label">First Name</label>
          <input type="text" class="form-control" id="EmployFirstName" aria-describedby="EmployFirstName" name="first_name">
          <!-- <div id="emailHelp" class="form-text">We'll never share your email with anyone else.</div> -->
        </div>
        <div class="mb-3">
          <label for="lastName" class="form-label">Last Name</label>
          <input type="text" class="form-control" id="EmployLastName" name="last_name">
          <!-- <div id="emailHelp" class="form-text">We'll never share your email with anyone else.</div> -->
        </div>
        <div class="mb-3">
          <label for="Position" class="form-label">Position</label>
          <select name="position" id="position" class="form-control">
            <option value="">--Choose One--</option>
            <option value="1">Staff</option>
            <option value="2">Office Boy</option>
            <option value="3">Secertary</option>
            <option value="4">Administrator</option>
          </select>
          <!-- <div id="emailHelp" class="form-text">We'll never share your email with anyone else.</div> -->
        </div>
        <div class="mb-3">
          <label for="office" class="form-label">Office</label>
          <select name="office" id="office" class="form-control">
            <option value="">--Choose One--</option>
            <option value="Jakarta">Jakarta</option>
            <option value="Bogor">Bogor</option>
            <option value="Bandung">Bandung</option>
            <option value="Surabaya">Surabaya</option>
          </select>
          <!-- <div id="emailHelp" class="form-text">We'll never share your email with anyone else.</div> -->
        </div>
        <div class="mb-3 form-check">
          <input type="checkbox" disabled class="form-check-input" id="exampleCheck1">
          <label class="form-check-label" for="exampleCheck1">Send email</label>
        </div>
        <!-- <button type="cancel" style="float:right;" class="btn btn-success btn-sm">Cancel</button> -->
        <div class="mb-3" style="float:right;">
          <button type="submit" class="btn btn-success btn-sm">Save</button>
          <a href="listdata.php" class="btn btn-danger btn-sm">Cancel</a>
        </div>
      </form>

    </div>

  </div>
<?php } ?>

<?php 
  include '../template/footer.php'; 
?>
  
