<?php 
  include '../connection/endpoint.php';
  $db = new Endpoint();
  include '../template/header.php';
  
  $rowlimit = 10;
	$page = isset($_GET['page'])?(int)$_GET['page'] : 1;
	$first_page = ($page>1) ? ($page * $rowlimit) - $rowlimit : 0;	
  
  $previous = $page - 1;
	$next = $page + 1;
  
  $dataPagging = $db->getPagging('karyawan');
	$total_Pagging = $dataPagging;
	$total_pages = ceil($total_Pagging / $rowlimit);

  $getListTabel = $db->getTabelSelectedColWithPagging('karyawan', 'id_karyawan, first_name, last_name, position,  office', $first_page, $rowlimit);
?>

  <div class="content">

    <a href="formdata.php?id=" class="btn btn-primary btn-sm cr-new">Create New Employee</a>

    <table class="table table-success table-striped">

      <thead>
        <tr class="th-list">
          <th>No</th>
          <th>Employee name</th>
          <th>position</th>
          <th>Office</th>
          <th>Action</th>
        </tr>
      </thead>
      
      <!-- tbody -->
      <?php
        $no = $first_page + 1;
        foreach($getListTabel as $row) : 
      ?>
        <tr class="td-list">
          <td><?php echo $no++; ?></td> 
          <td><?php echo $row['first_name'].' '.$row['last_name']; ?></td>
          <td>
            <?php 
              if($row['position'] == '1'){
                echo 'Staff';
              }else if($row['position'] == '2'){
                echo 'Office Boy';
              }else if($row['position'] == '3'){
                echo 'Secertary';
              }else if($row['position'] == '4'){
                echo 'Administrator';
              }else{
                echo 'Not registered';
              }
            ?>
          </td>
          <td><?php echo $row['office']; ?></td>
          <td>
            <a href="formdata.php?id=<?php echo $row['id_karyawan']; ?>" class="btn btn-warning btn-sm">edit</a>
            &nbsp;&nbsp;
            <a href="deleteproses.php?id=<?php echo $row['id_karyawan']; ?>"  class="btn btn-danger btn-sm">delete
            </a>
          </td>
        </tr>
      <?php
        endforeach;
      ?>
      <!-- end -->
    </table>

    <!-- pagging -->
    <nav aria-label="Page navigation example">
      <ul class="pagination justify-content-center">
        <li class="page-item">
					<a class="page-link" <?php if($page > 1){ echo "href='?page=$previous'"; } ?>>Previous</a>
				</li>
				<?php 
				for($x=1;$x<=$total_pages;$x++){
					?> 
					<li class="page-item"><a class="page-link" href="?page=<?php echo $x ?>"><?php echo $x; ?></a></li>
					<?php
				}
				?>				
				<li class="page-item">
					<a  class="page-link" <?php if($page < $total_pages) { echo "href='?page=$next'"; } ?>>Next</a>
				</li>
      </ul>
    </nav>

  </div>

<?php 
  include '../template/footer.php'; 
?>
  
