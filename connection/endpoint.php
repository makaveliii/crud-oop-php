<?php

include('connect.php');

class Endpoint extends Connect
{

  function __construct()
  {
    parent::__construct();
  }

  function get($tabel)
  {
    $query  = "SELECT * FROM $tabel ";
    $result = $this->conn->query($query);

    while ($resultx = $result->fetch_assoc()) {
      $row[] = $resultx;
    }
    return $row;
  }

  function getPagging($tabel)
  {
    $query  = "SELECT * FROM $tabel ";
    $result = $this->conn->query($query);
    return $result->num_rows;
  }

  function getById($tabel, $where, $id)
  {
    $query  = "SELECT * FROM $tabel WHERE  $where = '". $id ."'";
    $result = $this->conn->query($query);
    return $result->fetch_assoc();
  }

  function get_name($name, $tabel, $where, $values)
  {
    $query  = "SELECT $name FROM $tabel WHERE  $where = '". $values ."' ";
    $result = $this->conn->query($query);
    return $result->fetch_assoc();
  }

  function delete($tabel, $where, $id)
  {
    $query = "DELETE FROM $tabel WHERE $where = '".$id."' ";
    $result = $this->conn->query($query);

    if ($result) {
      # code...
      return true;
    }else{
      return false;
    }
  }

  function getTabelSelectedCol($tabel, $col)
  {
    $query  = "SELECT $col FROM $tabel";
    $result = $this->conn->query($query);
    while ($resultx = $result->fetch_assoc()) {
      $row[] = $resultx;
    }
    return $row;
  }

  function getTabelSelectedColWithPagging($tabel, $col, $page, $rows)
  {
    $query  = "SELECT $col FROM $tabel LIMIT $page, $rows";
    $result = $this->conn->query($query);
    while ($resultx = $result->fetch_assoc()) {
      $row[] = $resultx;
    }
    return $row;
  }

  function insertKaryawan($tabel, $data)
  {

    $date = date('Y-m-d');
    $prep_data = '';
    foreach($data as $v ) {
      $prep_data .= "'$v'".',';
    }
    $implode_prep_col = implode(', ', array_keys($data));
    $finals_value = substr_replace($prep_data ,"", -1);
    // $query = "INSERT INTO $tabel (first_name, last_name, position, office, start_date ) VALUES ('".$data['first_name']."' , '".$data['last_name']."' , '".$data['position']."' , '".$data['office']."', $date)";
    $query = "INSERT INTO $tabel ($implode_prep_col) VALUES ($finals_value)";
    $result = $this->conn->query($query);
    if ($result == true) {
      # code...
      return 1;
    }else{
      printf("%s\n", $this->conn->error);
      exit();
      return 0;
    }
  }

  function updateKaryawan($tabel, $data)
  {
    // print_r($data); die();
    $date = date('Y-m-d');
    $query = "UPDATE 
              $tabel 
              SET first_name = '".$data['first_name']."',
              last_name = '".$data['last_name']."',
              position = '".$data['position']."',
              office = '".$data['office']."'
              WHERE id_karyawan = '".$data['id']."' ";
    $result = $this->conn->query($query);
    if ($result == true) {
      # code...
      return 1;
    }else{
      printf("%s\n", $this->conn->error);
      exit();
      return 0;
    }
  }

}